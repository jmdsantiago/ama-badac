/**
 * Created by john.santiago on 07/09/2016.
 */

var ChartsAmcharts = function () {

    var chartData = [];
    var brgyName = $('#brgyusracc').text();

    var AJAXPost = function (sendURL, sendData) {
        $.post({
            url: sendURL,
            data: {sendData: sendData},
            success: function (data) {
                if (jQuery.isEmptyObject(data)) {
                    toastr.warning("An AJAX error occured. Please check the value you just edited.", 'Warning', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                } else {
                    chartData = data;
                    console.log(chartData);
                    chartData = JSON.parse(chartData);
                    console.log(jQuery.type(chartData));
                    userFormCountChart();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Server status: " + xhr.status);
                console.log("Server response: " + xhr.responseText);
                console.log("Server error: " + thrownError);
            }
        });
    };

    var getChartData = function(sentUrl, callback) {
        $.ajax({
            url: sentUrl,
            type: 'GET',
            data: {id: $('#brgyusracc').val()},
            success: function(data) {
                chartData = data;
                console.log(chartData);
                chartData = JSON.parse(chartData);
                console.log(jQuery.type(chartData));
                callback(chartData);
            }
        });
    };

    var userFormCountChart = function () {

        var chart = AmCharts.makeChart("user_formCount_chart", {

            "type": "serial",
            "categoryField": "barangay",
            "angle": 30,
            "depth3D": 30,
            "startDuration": 1,
            "startEffect": "easeOutSine",
            "export": {
                "enabled": true
            },
            "categoryAxis": {
                "gridPosition": "start",
                "autoGridCount": false,
                "gridCount": chartData.length
            },
            "chartCursor": {
                "enabled": true
            },
            "chartScrollbar": {
                "enabled": true
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "You have encoded [[value]] forms for [[category]]",
                    "fillAlphas": 1,
                    "id": "user_formCount_chart_graph",
                    "title": "Count",
                    "type": "column",
                    "valueField": "count"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "user_formCount_chart_axis",
                    "title": "Count"
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [
                {
                    "id": "user_formCount_chart",
                    "size": 15,
                    "text": "Per Barangay"
                }
            ],
            "dataProvider": chartData,
            "theme": "light",
            "pathToImages": "assets/global/plugins/amcharts/amcharts/images/"
        });
    };

    var brgy_admtype = function () {

        var chart = AmCharts.makeChart("rep_natdm", {

            "type": "serial",
            "categoryField": "value",
            "angle": 30,
            "depth3D": 30,
            "startDuration": 1,
            "startEffect": "easeOutSine",
            "export": {
                "enabled": true
            },
            "categoryAxis": {
                "gridPosition": "start",
                "autoGridCount": false,
                "gridCount": chartData.length
            },
            "chartCursor": {
                "enabled": true
            },
            "chartScrollbar": {
                "enabled": true
            },
            "trendLines": [],
            "graphs": [
                {
                    "balloonText": "[[value]] drug users in [[category]]",
                    "fillAlphas": 1,
                    "id": "rep_natdm_graph",
                    "title": "Count",
                    "type": "column",
                    "valueField": "count"
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "rep_natdm_axis",
                    "title": "Count"
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [
                {
                    "id": "rep_natdm",
                    "size": 15,
                    "text": "Nature of Admission"
                }
            ],
            "dataProvider": chartData,
            "theme": "light",
            "pathToImages": "assets/global/plugins/amcharts/amcharts/images/"
        });

    };

    return {
        //main function to initiate the module

        init: function () {

            AJAXPost('assets/backend/charts/userChart_formsPerBarangay.php', Cookies.get('usr_id'));

            $("#btn_genrep").click(function () {
                getChartData('assets/backend/charts/admtype_brgy.php', function (result) {
                    brgy_admtype();
                });
            });
        }

    };

}();

jQuery(document).ready(function () {
    ChartsAmcharts.init();
});