var formIntakeEditing = function () {

    // Function for displaying hidden input fields in "Other" checkboxes
    var showHideOtherInputFieldsChk = function (mainInput, fieldHide) {
        $("#" + mainInput).click(function () {
            if ($("#" + mainInput).is(':checked')) {
                $("#" + fieldHide).show();
                $("#" + fieldHide).attr("disabled", false);
            } else {
                $("#" + fieldHide).hide();
                $("#" + fieldHide).attr("disabled", true);
            }
        });

    };
    // Function for hiding input fields in "Other" radioboxes
    var hideOtherInputFieldsRad = function (mainInput, fieldHide) {
        $("#" + mainInput).click(function () {
            if ($("#" + mainInput).is(':checked')) {
                $("#" + fieldHide).hide();
                $("#" + fieldHide).attr("disabled", true);
            }
        });
    };
    // Function for showing input fields in "Other" radioboxes
    var showOtherInputFieldsRad = function (mainInput, fieldHide) {
        $("#" + mainInput).click(function () {
            if ($("#" + mainInput).is(':checked')) {
                $("#" + fieldHide).show();
                $("#" + fieldHide).attr("disabled", false);
            }
        });
    };

    // Function for submitting arrays via AJAX
    var postAJAXArray = function (sendURL, sendData) {
        $.post({
            url: sendURL,
            data: {sendData: sendData},
            success: function (data) {
                if (jQuery.isEmptyObject(data)) {
                    toastr.warning("An AJAX error occured. Please check the value you just edited.", 'Warning', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                } else {
                    toastr.info('Notice: A part of the form has been processed by the server.', 'Debugging mode', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Server status: " + xhr.status);
                console.log("Server response: " + xhr.responseText);
                console.log("Server error: " + thrownError);
            }
        });
    };
    // Function to pass multiple choice elements to arrays then to AJAX
    var pass2Array = function (elementData, personIDVal, tblVal, submitURL) {
        var Arr = [];
        if (($('#' + elementData).is(":checkbox") || $('#' + elementData).is(":radio")) && $('#' + elementData).is(":visible")) {
            $('#' + elementData).each(function () {
                if ($(this).is(':checked')) {
                    Arr.push({
                        num_id: '',
                        person_id: personIDVal,
                        [tblVal]: $(this).val()
                    });
                } else { //remove this in production (DEBUGGING PURPOSE LANG)
                    toastr.info("\"" + elementData + "\" does not have any checked values.", 'Debugging mode', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                }
            });
            postAJAXArray(submitURL, Arr);
        } else if (($('#' + elementData).is(":selected") || ($('#' + elementData).is(":text") && $('#' + elementData).not(":disabled"))) && $('#' + elementData).is(":visible")) {
            $('#' + elementData).each(function () {
                Arr.push({
                    num_id: '',
                    person_id: personIDVal,
                    [tblVal]: $(this).val()
                });
            });
            postAJAXArray(submitURL, Arr);
        } else { //remove this in production (DEBUGGING PURPOSE LANG)
            toastr.info("\"" + elementData + "\" is not a valid checkbox,radiobox, or select field.", 'Debugging mode', {
                timeOut: 9000,
                extendedTimeOut: 9000
            });
        }
    };

    var getUserData = function (url, type) {
        $.get(url, function (data, status) {
            type = data;
        });
    };

    return {
        //main function to initiate the module
        init: function () {

            // Hide "Other" input boxes on module initialization
            $("#person_livarother").hide();
            $("#person_sourcedrugother").hide();
            $("#person_natadmother").hide();
            $("#person_numadmother").hide();
            $("#val_trt_freqother").hide();

            // Show/hide "Other" in checkboxes
            showHideOtherInputFieldsChk('person_livar7', 'person_livarother');
            showHideOtherInputFieldsChk('person_sourcedrug6', 'person_sourcedrugother');
            showHideOtherInputFieldsChk('person_natadm7', 'person_natadmother');

            // Show/hide "Other" in radio boxes
            hideOtherInputFieldsRad('person_freqdrug1', 'val_trt_freqother');
            hideOtherInputFieldsRad('person_freqdrug2', 'val_trt_freqother');
            hideOtherInputFieldsRad('person_freqdrug3', 'val_trt_freqother');
            hideOtherInputFieldsRad('person_freqdrug4', 'val_trt_freqother');
            hideOtherInputFieldsRad('person_freqdrug5', 'val_trt_freqother');
            showOtherInputFieldsRad('person_freqdrug6', 'val_trt_freqother');

            // Form submission
            $("#form_edited_submit").click(function () {
                var personID;
                var submitArrayPersonData = [];
                var key;
                // Get person ID from control number
                personID = $('#frm_edit_ctrlnum').val();
                $('#formEditContainer').find('div').each(function () {
                    // Loop through divs and check which one is not hidden
                    if ($(this).is(":hidden")) {
                        //console.log("hidden div: " + this.id + " found.");
                    } else {
                        // Find input element within div
                        var getVisibleInput = this.id;
                        if (getVisibleInput == "person_livar") {
                            submitArrayPersonData.push({
                                elem: 'person_livar input:checked',
                                pid: personID,
                                valueid: 'val_name',
                                url: 'assets/backend/intakeForm/edit_person_livar_chk.php'
                            });
                            if ($('#person_livar input[type="checkbox"]').eq(6).prop('checked') === true) {
                                submitArrayPersonData.push({
                                    elem: 'person_livarother',
                                    pid: personID,
                                    valueid: 'val_name',
                                    url: 'assets/backend/intakeForm/edit_person_livar_other.php'
                                });
                            } else {
                                submitArrayPersonData.push({person_id: personID});
                                postAJAXArray('assets/backend/intakeForm/del_person_livar_other.php', submitArrayPersonData);
                            }
                            for (key in submitArrayPersonData) {
                                pass2Array(submitArrayPersonData[key]['elem'], submitArrayPersonData[key]['pid'], submitArrayPersonData[key]['valueid'], submitArrayPersonData[key]['url']);
                            }
                        } else if (getVisibleInput == "person_sourcedrug") {
                            submitArrayPersonData.push({
                                elem: 'person_sourcedrug input:checked',
                                pid: personID,
                                valueid: 'val_name',
                                url: 'assets/backend/intakeForm/edit_person_sourcedrug.php'
                            });
                            if ($('#person_sourcedrug input[type="radio"]').eq(5).prop('checked') === true) {
                                submitArrayPersonData.push({
                                    elem: 'person_sourcedrugother',
                                    pid: personID,
                                    valueid: 'val_name',
                                    url: 'assets/backend/intakeForm/edit_person_sourcedrugother.php'
                                });
                            } else {
                                submitArrayPersonData.push({person_id: personID});
                                postAJAXArray('assets/backend/intakeForm/del_person_sourcedrug.php', submitArrayPersonData);
                            }
                            for (key in submitArrayPersonData) {
                                pass2Array(submitArrayPersonData[key]['elem'], submitArrayPersonData[key]['pid'], submitArrayPersonData[key]['valueid'], submitArrayPersonData[key]['url']);
                            }
                        } else if (getVisibleInput == "person_natadm") {
                            submitArrayPersonData.push({
                                elem: 'person_natadm input:checked',
                                pid: personID,
                                valueid: 'val_name',
                                url: 'assets/backend/intakeForm/edit_person_natadm.php'
                            });
                            if ($('#person_natadm input[type="checkbox"]').eq(6).prop('checked') === true) {
                                submitArrayPersonData.push({
                                    elem: 'person_natadmother',
                                    pid: personID,
                                    valueid: 'val_name',
                                    url: 'assets/backend/intakeForm/edit_person_natadmother.php'
                                });
                            } else {
                                submitArrayPersonData.push({person_id: personID});
                                postAJAXArray('assets/backend/intakeForm/del_person_natadmother.php', submitArrayPersonData);
                            }
                            for (key in submitArrayPersonData) {
                                pass2Array(submitArrayPersonData[key]['elem'], submitArrayPersonData[key]['pid'], submitArrayPersonData[key]['valueid'], submitArrayPersonData[key]['url']);
                            }
                        } else if (getVisibleInput == "val_trt_freq") {
                            submitArrayPersonData.push({
                                elem: 'val_trt_freq input:checked',
                                pid: personID,
                                valueid: 'val_name',
                                url: 'assets/backend/intakeForm/edit_person_drugsub.php'
                            });
                            if ($('#val_trt_freq input[type="radio"]').eq(5).prop('checked') === true) {
                                submitArrayPersonData.push({
                                    elem: 'val_trt_freqother',
                                    pid: personID,
                                    valueid: 'val_name',
                                    url: 'assets/backend/intakeForm/edit_person_freqdrugother.php'
                                });
                            } else {
                                submitArrayPersonData.push({person_id: personID});
                                postAJAXArray('assets/backend/intakeForm/del_person_freqdrugother.php', submitArrayPersonData);
                            }
                            for (key in submitArrayPersonData) {
                                pass2Array(submitArrayPersonData[key]['elem'], submitArrayPersonData[key]['pid'], submitArrayPersonData[key]['valueid'], submitArrayPersonData[key]['url']);
                            }
                        } else if (getVisibleInput == "person_drugsused") {
                            submitArrayPersonData.push({
                                elem: 'person_drugsused option:selected',
                                pid: personID,
                                valueid: 'val_name',
                                url: 'assets/backend/intakeForm/edit_person_drugsused.php'
                            });
                            //console.log(submitArrayPersonData);
                            for (key in submitArrayPersonData) {
                                pass2Array(submitArrayPersonData[key]['elem'], submitArrayPersonData[key]['pid'], submitArrayPersonData[key]['valueid'], submitArrayPersonData[key]['url']);
                            }
                        } else if (getVisibleInput == "person_drugsub") {
                            submitArrayPersonData.push({
                                elem: 'person_drugsub option:selected',
                                pid: personID,
                                valueid: 'val_name',
                                url: 'assets/backend/intakeForm/edit_person_drugsub.php'
                            });
                            //console.log(submitArrayPersonData);
                            for (key in submitArrayPersonData) {
                                pass2Array(submitArrayPersonData[key]['elem'], submitArrayPersonData[key]['pid'], submitArrayPersonData[key]['valueid'], submitArrayPersonData[key]['url']);
                            }
                        } else if (getVisibleInput == "val_ctrl_brgy" || getVisibleInput == "val_bg_sex" || getVisibleInput == "val_bg_civstat" || getVisibleInput == "val_bg_edu" || getVisibleInput == "val_bg_nation" || getVisibleInput == "val_bg_relg" || getVisibleInput == "val_work_prerehab" || getVisibleInput == "val_fam_ordinal" || getVisibleInput == "val_fam_dadocc" || getVisibleInput == "val_fam_momocc" || getVisibleInput == "val_fam_spouseocc") {
                            //console.log(getVisibleInput);
                            var getSelectValue;
                            getSelectValue = $("#" + getVisibleInput + '_edit').select2('data').id;
                            //console.log(getSelectValue);
                            editIntakeForm = [];
                            editIntakeForm.push({
                                person_id: personID,
                                getVisibleInput: getVisibleInput,
                                valueToUpdate: getSelectValue
                            });
                            //console.log(editIntakeForm);
                            postAJAXArray('assets/backend/intakeForm/edit_mainForm.php', editIntakeForm);
                            toastr.info("Updated an input field.", 'Debugging mode', {
                                timeOut: 9000,
                                extendedTimeOut: 9000
                            });
                        } else if ($("#" + getVisibleInput + ":has(input)")) {
                            var getInputValue;
                            getInputValue = $("#" + getVisibleInput + " input").val();
                            //console.log(getInputValue);
                            var editIntakeForm = [];
                            editIntakeForm.push({
                                person_id: personID,
                                getVisibleInput: getVisibleInput,
                                valueToUpdate: getInputValue
                            });
                            //console.log(editIntakeForm);
                            postAJAXArray('assets/backend/intakeForm/edit_mainForm.php', editIntakeForm);
                            toastr.info("Updated an input field.", 'Debugging mode', {
                                timeOut: 9000,
                                extendedTimeOut: 9000
                            });
                        } else {
                            toastr.error("Something went wrong. Please refresh the page immediately.", 'Error', {
                                timeOut: 9000,
                                extendedTimeOut: 9000
                            });
                        }
                    }
                });
                //submitArrayPersonData.push({ elem: 'val_trt_freq input:checked', pid: personID, valueid: 'nnnnnn', url: 'assets/backend/intakeForm/val_trt_freq.php' });
            });
        }
    };
}();

jQuery(document).ready(function () {
    formIntakeEditing.init();
});
