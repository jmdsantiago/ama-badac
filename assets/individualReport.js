var userReportIndiv = function() {

    var getSelectAll = function(getSelectBox) {
        $("#usr_rep_names").select2({
            ajax: {
                url: "assets/backend/reports/individual/select_all.php",
                tokenSeparators: [',', ' '],
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(obj) {
                            return { id: obj.person_id, text: obj.val_bg_fname + " " + obj.val_bg_mname + " " + obj.val_bg_lname };
                        })
                    };
                }
            }
        });
    };

    var getSelectDataLimited = function(getSelectBox) {
        $("#usr_rep_names").select2({
            ajax: {
                url: "assets/backend/reports/individual/select_limited.php",
                tokenSeparators: [',', ' '],
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        w: Cookies.get('usr_id')
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(obj) {
                            return { id: obj.person_id, text: obj.val_bg_fname + " " + obj.val_bg_mname + " " + obj.val_bg_lname };
                        })
                    };
                }
            }
        });
    };

    var getValue_Select = function(theElement, theTable) {
        $.ajax({
            url: "assets/backend/reports/individual/getValue.php",
            data: {id: $("#" + theElement).text(), tbl: theTable},
            success : function(data) {
                $("#" + theElement).text(data[0]['val_name']);
            }
        });
    };

    var getValue = function(theElement, theData, theTable) {
        $.ajax({
            url: "assets/backend/reports/individual/getValue.php",
            data: {id: theData, tbl: theTable},
            success : function(data) {
                for(var i = 0; i < data.length; i++) {
                    $("#" + theElement).append('<p>'+data[i]['val_name']+'</p>');
                }

            }
        });
    };

    var getMultiple = function(theElement, theValue, theTable) {
      $.ajax({
          url: "assets/backend/reports/individual/getMultiple.php",
          data: {id: theValue, tbl: theTable},
          success : function(data) {
              for(var i = 0; i < data.length; i++) {
                  if (theElement == 'person_livar') {
                      if (data[i]['val_name'] == 'WP') {
                          $("#" + theElement).append('<p>With parents</p>');
                      } else if (data[i]['val_name'] == 'WR') {
                          $("#" + theElement).append('<p>With relatives</p>');
                      } else if (data[i]['val_name'] == 'BH') {
                          $("#" + theElement).append('<p>Boarding house</p>');
                      } else if (data[i]['val_name'] == 'WSC') {
                          $("#" + theElement).append('<p>With spouse and children</p>');
                      } else if (data[i]['val_name'] == 'WF') {
                          $("#" + theElement).append('<p>With friends</p>');
                      } else if (data[i]['val_name'] == 'LA') {
                          $("#" + theElement).append('<p>Living alone</p>');
                      } else if (data[i]['val_name'] == 'O') {
                          $("#" + theElement).append('<p>Others</p>');
                      } else {
                          $("#" + theElement).append('<p>Warning: Data corruption</p>');
                      }
                  } else if (theElement == 'person_sourcedrug') {
                      if (data[i]['val_name'] == 'F') {
                          $("#" + theElement).append('<p>Friend/Peer</p>');
                      } else if (data[i]['val_name'] == 'P') {
                          $("#" + theElement).append('<p>Pusher</p>');
                      } else if (data[i]['val_name'] == 'S') {
                          $("#" + theElement).append('<p>Self</p>');
                      } else if (data[i]['val_name'] == 'D') {
                          $("#" + theElement).append('<p>Drugstore</p>');
                      } else if (data[i]['val_name'] == 'R') {
                          $("#" + theElement).append('<p>Relatives</p>');
                      } else if (data[i]['val_name'] == 'O') {
                          $("#" + theElement).append('<p>Others</p>');
                      } else {
                          $("#" + theElement).append('<p>Warning: Data corruption</p>');
                      }
                  }  else if (theElement == 'person_natadm') {
                      if (data[i]['val_name'] == 'V') {
                          $("#" + theElement).append('<p>Voluntary</p>');
                      } else if (data[i]['val_name'] == 'VS') {
                          $("#" + theElement).append('<p>Voluntary Surrender</p>');
                      } else if (data[i]['val_name'] == 'A') {
                          $("#" + theElement).append('<p>Arrested</p>');
                      } else if (data[i]['val_name'] == 'SS') {
                          $("#" + theElement).append('<p>Suspended Sentence</p>');
                      } else if (data[i]['val_name'] == 'R') {
                          $("#" + theElement).append('<p>Relapse/Readmission</p>');
                      } else if (data[i]['val_name'] == 'CC') {
                          $("#" + theElement).append('<p>Compulsory Confinement</p>');
                      } else if (data[i]['val_name'] == 'O') {
                          $("#" + theElement).append('<p>Others</p>');
                      } else {
                          $("#" + theElement).append('<p>Warning: Data corruption</p>');
                      }
                  } else if (theElement == 'person_drugsub') {
                      getValue('person_drugsub', data[i]['val_name'], 'tbl_drug_substance');
                  }
                  else if (theElement == 'person_drugsused') {
                      getValue('person_drugsused', data[i]['val_name'], 'tbl_drugs');
                  } else {
                      $("#" + theElement).append('<p>Warning: Data error</p>');
                  }
              }
              //
          }
      });
    };

    var reportValues = function() {
        $.ajax({
            type: "POST",
            url: "assets/backend/reports/individual/getFormValues.php",
            data: {id: $("#usr_rep_names").select2("val")},
            success : function(data)
            {
                $('#form_person_num').text(data[0]['person_id']);

                var person_num = $('#form_person_num').text();

                var canvas = document.getElementById("val_ctrl_photo");
                var ctx = canvas.getContext("2d");

                var image = new Image();
                image.onload = function() {
                    ctx.drawImage(image, 0, 0);
                };
                image.src = "assets/intakeForm/data/images/" + data[0]['val_ctrl_brgy'] + '/' + $('#form_person_num').text() + '.jpg';
                image.style.width = 320;
                image.style.height = 240;



                $('#val_bg_fname').text(data[0]['val_bg_fname']);
                $('#val_bg_lname').text(data[0]['val_bg_lname']);
                $('#val_ctrl_brgy').text(data[0]['val_ctrl_brgy']);
                getValue_Select('val_ctrl_brgy','tbl_barangays');
                $('#val_ctrl_dateadm').text(data[0]['val_ctrl_dateadm']);
                $('#val_bg_addr').text(data[0]['val_bg_addr']);

                if (data[0]['val_bg_type'] == 'L') {
                    $('#val_bg_type').text("Listed");
                } else if (data[0]['val_bg_type'] == 'NA') {
                    $('#val_bg_type').text("Not Applicable");
                } else if (data[0]['val_bg_type'] == 'U') {
                    $('#val_bg_type').text("User");
                } else if (data[0]['val_bg_type'] == 'P') {
                    $('#val_bg_type').text("Pusher");
                } else if (data[0]['val_bg_type'] == 'W') {
                    $('#val_bg_type').text("Watchlist");
                } else if (data[0]['val_bg_type'] == 'N') {
                    $('#val_bg_type').text("Not Listed");
                } else {
                    $('#val_bg_type').text("Data error");
                }

                $('#val_bg_contact').text(data[0]['val_bg_contact']);
                $('#val_bg_dob').text(data[0]['val_bg_dob']);
                $('#val_bg_email').text(data[0]['val_bg_email']);
                $('#val_bg_surr_date').text(data[0]['val_bg_surr_date']);
                $('#val_bg_surr_time').text(data[0]['val_bg_surr_time']);
                $('#val_bg_age').text(data[0]['val_bg_age']);

                if (data[0]['val_bg_sex'] == 'Male') {
                    $('#val_bg_sex').text("Male");
                } else if (data[0]['val_bg_sex'] == 'Female') {
                    $('#val_bg_sex').text("Female");
                } else {
                    $('#val_bg_sex').text("Data error");
                }

                if (data[0]['val_bg_civstat'] == 'S') {
                    $('#val_bg_civstat').text("Single");
                } else if (data[0]['val_bg_civstat'] == 'M') {
                    $('#val_bg_civstat').text("Married");
                } else if (data[0]['val_bg_civstat'] == 'L') {
                    $('#val_bg_civstat').text("Live-in");
                } else if (data[0]['val_bg_civstat'] == 'W') {
                    $('#val_bg_civstat').text("Widowed");
                } else if (data[0]['val_bg_civstat'] == 'S') {
                    $('#val_bg_civstat').text("Separated");
                } else if (data[0]['val_bg_civstat'] == 'D') {
                    $('#val_bg_civstat').text("Divorced");
                } else {
                    $('#val_bg_civstat').text("Data error");
                }

                if (data[0]['val_bg_edu'] == 'N') {
                    $('#val_bg_edu').text("Not applicable");
                } else if (data[0]['val_bg_edu'] == 'E') {
                    $('#val_bg_edu').text("Elementary");
                } else if (data[0]['val_bg_edu'] == 'HS') {
                    $('#val_bg_edu').text("High School (Before K-12)");
                } else if (data[0]['val_bg_edu'] == 'JH') {
                    $('#val_bg_edu').text("Junior High");
                } else if (data[0]['val_bg_edu'] == 'SH') {
                    $('#val_bg_edu').text("Senior High");
                } else if (data[0]['val_bg_edu'] == 'V') {
                    $('#val_bg_edu').text("Vocational");
                } else if (data[0]['val_bg_edu'] == 'C') {
                    $('#val_bg_edu').text("College Degree");
                } else if (data[0]['val_bg_edu'] == 'M') {
                    $('#val_bg_edu').text("Masters Degree");
                } else if (data[0]['val_bg_edu'] == 'D') {
                    $('#val_bg_edu').text("Doctorate Degree");
                } else {
                    $('#val_bg_edu').text("Data error");
                }

                $('#val_bg_nation').text(data[0]['val_bg_relg']);
                getValue_Select('val_bg_nation','tbl_fam_nationality');
                $('#val_bg_relg').text(data[0]['val_bg_relg']);
                getValue_Select('val_bg_relg','tbl_fam_religion');
                $('#val_school_yrs').text(data[0]['val_school_yrs']);
                $('#val_school_date').text(data[0]['val_school_date']);
                $('#val_work_prerehab').text(data[0]['val_work_prerehab']);
                getValue_Select('val_work_prerehab','tbl_fam_occupation');
                $('#val_bg_skill').text(data[0]['val_bg_skill']);
                $('#val_fam_siblings').text(data[0]['val_fam_siblings']);

                if (data[0]['val_fam_ordinal'] == 'M') {
                    $('#val_fam_ordinal').text("Mother");
                } else if (data[0]['val_fam_ordinal'] == 'F') {
                    $('#val_fam_ordinal').text("Father");
                } else if (data[0]['val_fam_ordinal'] == 'EC') {
                    $('#val_fam_ordinal').text("Eldest Child");
                } else if (data[0]['val_fam_ordinal'] == 'MC') {
                    $('#val_fam_ordinal').text("Middle Child");
                } else if (data[0]['val_fam_ordinal'] == 'YC') {
                    $('#val_fam_ordinal').text("Youngest Child");
                } else {
                    $('#val_fam_ordinal').text("Data error");
                }

                $('#person_livar').text(' ');
                getMultiple('person_livar', person_num, 'tbl_person_livarr');

                $('#val_fam_income').text(data[0]['val_fam_income']);
                $('#val_fam_dadname').text(data[0]['val_fam_dadname']);
                $('#val_fam_dadocc').text(data[0]['val_fam_dadocc']);
                getValue_Select('val_fam_dadocc','tbl_fam_occupation');
                $('#val_fam_momname').text(data[0]['val_fam_momname']);
                $('#val_fam_momocc').text(data[0]['val_fam_momocc']);
                getValue_Select('val_fam_momocc','tbl_fam_occupation');
                $('#val_fam_spousename').text(data[0]['val_fam_spousename']);
                $('#val_fam_spouseocc').text(data[0]['val_fam_spouseocc']);
                getValue_Select('val_fam_spouseocc','tbl_fam_occupation');
                $('#val_fam_spouse_addr').text(data[0]['val_fam_spouse_addr']);
                $('#val_drg_expense').text(data[0]['val_drg_expense']);
                $('#val_drg_agefirst').text(data[0]['val_drg_agefirst']);
                $('#val_drg_datelast').text(data[0]['val_drg_datelast']);

                if (data[0]['val_drg_lengthuse'] == 'L2') {
                    $('#val_drg_lengthuse').text("Less than 2 years");
                } else if (data[0]['val_drg_lengthuse'] == 'EoG2L4') {
                    $('#val_drg_lengthuse').text("Equal or greater than 2 years but less than 4 years");
                } else if (data[0]['val_drg_lengthuse'] == 'EoG4L6') {
                    $('#val_drg_lengthuse').text("Equal or greater than 4 years but less than 6 years");
                } else if (data[0]['val_drg_lengthuse'] == 'EoG6') {
                    $('#val_drg_lengthuse').text("Equal or greater than 6 years");
                } else {
                    $('#val_drg_lengthuse').text("Data error");
                }

                $('#person_sourcedrug').text(' ');
                getMultiple('person_sourcedrug', person_num, 'tbl_person_drugsource');
                $('#val_drg_place').text(data[0]['val_drg_place']);
                $('#val_drg_reason').text(data[0]['val_drg_reason']);
                $('#person_drugsused').text(' ');
                getMultiple('person_drugsused', person_num, 'tbl_person_drugused'); // Please fix this
                $('#person_natadm').text(' ');
                getMultiple('person_natadm', person_num, 'tbl_person_admission');

                if (data[0]['val_trt_numadm'] == 'N') {
                    $('#val_trt_numadm').text("None");
                } else if (data[0]['val_trt_numadm'] == 'O') {
                    $('#val_trt_numadm').text("Once");
                } else if (data[0]['val_trt_numadm'] == 'T') {
                    $('#val_trt_numadm').text("Twice");
                } else if (data[0]['val_trt_numadm'] == 'MT') {
                    $('#val_trt_numadm').text("More than twice");
                } else {
                    $('#val_trt_numadm').text("Data error");
                }

                getMultiple('person_drugsub', person_num, 'tbl_person_substances'); // Please fix this

                if (data[0]['val_trt_freq'] == 'D') {
                    $('#val_trt_freq').text("Daily");
                } else if (data[0]['val_trt_freq'] == '2TO5') {
                    $('#val_trt_freq').text("2 to 5 times a week");
                } else if (data[0]['val_trt_freq'] == 'W') {
                    $('#val_trt_freq').text("Weekly");
                } else if (data[0]['val_trt_freq'] == 'OCC') {
                    $('#val_trt_freq').text("Occasionally");
                } else if (data[0]['val_trt_freq'] == 'M') {
                    $('#val_trt_freq').text("Monthly");
                } else if (data[0]['val_trt_freq'] == 'O') {
                    $('#val_trt_freq').text("Other");
                }  else {
                    $('#val_trt_freq').text("Data error");
                }

                $('#val_trt_means').text(data[0]['val_trt_means']);


            }
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            var usrLevel = Cookies.get('lvlusracc');
            // force refresh select data manually
            $("#frm_refresh_select").click(function() {
                if (usrLevel == 0) {
                    $('.form-control.select2dynamic').each(function(i, obj) {
                        getSelectAll();
                    });
                } else {
                    $('.form-control.select2dynamic').each(function(i, obj) {
                        getSelectDataLimited();
                    });
                }
                toastr.success("Dropdown menu data has been updated.", 'Success', { timeOut: 9000, extendedTimeOut: 9000 });
            });

            $("#btn_genrep").click(function () {
                    reportValues();
            });

        }

    };

}();

$(document).ready(function() {
    userReportIndiv.init();
});
