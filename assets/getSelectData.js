var selectDropdowns = function() {

    var getSelectData = function(getSelectBox) {
        $("#" + getSelectBox).select2({
            ajax: {
                url: "assets/backend/select/select_" + getSelectBox + ".php",
                tokenSeparators: [',', ' '],
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term // search term
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(obj) {
                            return { id: obj.val_id, text: obj.val_name };
                        })
                    };
                }
            }
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            // data initialized when control number is entered.
            $("#person_id").change(function() {
                // "i" is the position in the array, "obj" is the DOM object itself, $(this) is the jQuery object
                $('.form-control.select2dynamic').each(function(i, obj) {
                    getSelectData($(this).attr('id'));
                });
            });
            $("#nameusracc").change(function() {
                // "i" is the position in the array, "obj" is the DOM object itself, $(this) is the jQuery object
                $('.form-control.select2dynamic').each(function(i, obj) {
                    getSelectData($(this).attr('id'));
                });
            });
            // force refresh select data manually
            $("#frm_refresh_select").click(function() {
                toastr.success("Dropdown menu data has been updated.", 'Success', { timeOut: 9000, extendedTimeOut: 9000 });
                $('.form-control.select2dynamic').each(function(i, obj) {
                    getSelectData($(this).attr('id'));
                });
            });


        }

    };

}();

$(document).ready(function() {
    selectDropdowns.init();
});
