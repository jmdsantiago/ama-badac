var FormValidation = function () {

    // Variables for checking individual form validation; if all are 1 then pass entire form for submitting; if any are 0 then alert user for form checking
    var frm_aftercare_isValid = 0;
    var chkIdIfExists = 0;

    var frm_aftercare = function () {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var frm_aftercare_var = $('#frm_aftercare');
        var error2 = $('.alert-danger', frm_aftercare_var);
        var success2 = $('.alert-success', frm_aftercare_var);

        frm_aftercare_var.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                val_aftcr_client: {
                    required: true
                },
                val_aftcr_addr: {
                    required: true
                },
                val_aftcr_badac: {
                    required: true
                },
                val_aftcr_contact: {
                    required: true
                },
                val_aftcr_interviewer: {
                    required: true
                },
                val_aftcr_date: {
                    required: true
                },
                val_aftcr_notes: {
                    required: true
                },
                val_med_q1: {
                    required: true,
                    minlength: 1
                },
                val_med_q2: {
                    required: true,
                    minlength: 1
                },
                val_med_q3: {
                    required: true,
                    minlength: 1
                },
                val_med_q4: {
                    required: true,
                    minlength: 1
                },
                val_med_q5: {
                    required: true,
                    minlength: 1
                },
                val_med_q6: {
                    required: true,
                    minlength: 1
                },
                val_psych_q1: {
                    required: true,
                    minlength: 1
                },
                val_psych_q2: {
                    required: true,
                    minlength: 1
                },
                val_psych_q3: {
                    required: true,
                    minlength: 1
                },
                val_psych_q4: {
                    required: true,
                    minlength: 1
                },
                val_psych_q5: {
                    required: true,
                    minlength: 1
                },
                val_psych_q6: {
                    required: true,
                    minlength: 1
                },
                val_psych_q7: {
                    required: true,
                    minlength: 1
                },
                val_socio_q1: {
                    required: true,
                    minlength: 1
                },
                val_socio_q2: {
                    required: true,
                    minlength: 1
                },
                val_socio_q3: {
                    required: true,
                    minlength: 1
                },
                val_socio_q4: {
                    required: true,
                    minlength: 1
                },
                val_socio_q5: {
                    required: true,
                    minlength: 1
                },
                val_socio_q6: {
                    required: true,
                    minlength: 1
                },
                val_socio_q7: {
                    required: true,
                    minlength: 1
                },
                val_spirit_q1: {
                    required: true,
                    minlength: 1
                },
                val_spirit_q2: {
                    required: true,
                    minlength: 1
                },
                val_self_q1: {
                    required: true,
                    minlength: 1
                },
                val_self_q2: {
                    required: true,
                    minlength: 1
                },
                val_thera_q1: {
                    required: true,
                    minlength: 1
                },
                val_thera_q2: {
                    required: true,
                    minlength: 1
                },
                val_thera_q3: {
                    required: true,
                    minlength: 1
                },
                val_thera_q4: {
                    required: true,
                    minlength: 1
                },
                val_thera_q5: {
                    required: true,
                    minlength: 1
                }

            },

            messages: { // custom messages for radio buttons and checkboxes
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
                frm_aftercare_isValid = 0;
            },

            errorPlacement: function (error, element) { // render error placement for each input type

                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({
                        'container': 'body'
                    }); // for other inputs, just perform default behavior
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");

            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                frm_aftercare_isValid = 1;
                //form[0].submit(); // submit the form
            }
        });

        //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
        $('.select2me', frm_aftercare_var).change(function () {
            frm_aftercare_var.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

        //initialize datepicker
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
        $('.date-picker .form-control').change(function () {
            frm_aftercare_var.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

    };

    var handleTimePickers = function () {

        if (jQuery().timepicker) {
            $('.timepicker-default').timepicker({
                autoclose: true,
                showSeconds: true,
                minuteStep: 1
            });

            $('.timepicker-no-seconds').timepicker({
                autoclose: true,
                minuteStep: 5
            });

            $('.timepicker-24').timepicker({
                autoclose: true,
                minuteStep: 5,
                showSeconds: false,
                showMeridian: false
            });

            // handle input group button click
            $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function (e) {
                e.preventDefault();
                $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
            });

            // Workaround to fix timepicker position on window scroll
            $(document).scroll(function () {
                $('#form_modal4 .timepicker-default, #form_modal4 .timepicker-no-seconds, #form_modal4 .timepicker-24').timepicker('place'); //#modal is the id of the modal
            });
        }
    };

    // Function for submitting arrays via AJAX
    var postAJAXArray = function (sendURL, sendData) {
        $.post({
            url: sendURL,
            data: {sendData: sendData},
            success: function (data) {
                if (jQuery.isEmptyObject(data)) {
                    //toastr.warning("Some data may not have been sent properly. Please edit the form after this.", 'AJAX Error', { timeOut: 9000, extendedTimeOut: 9000 });
                } else {
                    //toastr.info('Notice: A part of the form has been processed by the server.', 'Debugging mode', { timeOut: 9000, extendedTimeOut: 9000 });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.error("The server sent an error. Please try filling out the data and try submitting again.", 'Error', {
                    timeOut: 9000,
                    extendedTimeOut: 9000
                });
            }
        });
    };

    var finalAftcrChk = function (url, value) {
        $.ajax({
            type: "POST",
            url: url,
            data: {id: value},
            success: function (data) {
                chkIdIfExists = data[0]['person_id'];
                if (chkIdIfExists == $('#val_aftcr_badac').val()) {

                    // Submission of main form
                    var afterCareIntakeForm = [];
                    afterCareIntakeForm.push({
                        val_aftcr_client: $('#val_aftcr_client').val(),
                        val_aftcr_addr: $('#val_aftcr_addr').val(),
                        val_aftcr_badac: $('#val_aftcr_badac').val(),
                        val_aftcr_contact: $('#val_aftcr_contact').val(),
                        val_aftcr_interviewer: $('#val_aftcr_interviewer').val(),
                        val_aftcr_date: $('#val_aftcr_date').val(),
                        val_aftcr_notes: $('#val_aftcr_notes').val(),
                        val_med_q1: $('#val_med_q1 input[type="radio"]:checked').val(),
                        val_med_q2: $('#val_med_q2 input[type="radio"]:checked').val(),
                        val_med_q3: $('#val_med_q3 input[type="radio"]:checked').val(),
                        val_med_q4: $('#val_med_q4 input[type="radio"]:checked').val(),
                        val_med_q5: $('#val_med_q5 input[type="radio"]:checked').val(),
                        val_med_q6: $('#val_med_q6 input[type="radio"]:checked').val(),
                        val_psych_q1: $('#val_psych_q1 input[type="radio"]:checked').val(),
                        val_psych_q2: $('#val_psych_q2 input[type="radio"]:checked').val(),
                        val_psych_q3: $('#val_psych_q3 input[type="radio"]:checked').val(),
                        val_psych_q4: $('#val_psych_q4 input[type="radio"]:checked').val(),
                        val_psych_q5: $('#val_psych_q5 input[type="radio"]:checked').val(),
                        val_psych_q6: $('#val_psych_q6 input[type="radio"]:checked').val(),
                        val_psych_q7: $('#val_psych_q7 input[type="radio"]:checked').val(),
                        val_socio_q1: $('#val_socio_q1 input[type="radio"]:checked').val(),
                        val_socio_q2: $('#val_socio_q2 input[type="radio"]:checked').val(),
                        val_socio_q3: $('#val_socio_q3 input[type="radio"]:checked').val(),
                        val_socio_q4: $('#val_socio_q4 input[type="radio"]:checked').val(),
                        val_socio_q5: $('#val_socio_q5 input[type="radio"]:checked').val(),
                        val_socio_q6: $('#val_socio_q6 input[type="radio"]:checked').val(),
                        val_socio_q7: $('#val_socio_q7 input[type="radio"]:checked').val(),
                        val_spirit_q1: $('#val_spirit_q1 input[type="radio"]:checked').val(),
                        val_spirit_q2: $('#val_spirit_q2 input[type="radio"]:checked').val(),
                        val_self_q1: $('#val_self_q1 input[type="radio"]:checked').val(),
                        val_self_q2: $('#val_self_q2 input[type="radio"]:checked').val(),
                        val_thera_q1: $('#val_thera_q1 input[type="radio"]:checked').val(),
                        val_thera_q2: $('#val_thera_q2 input[type="radio"]:checked').val(),
                        val_thera_q3: $('#val_thera_q3 input[type="radio"]:checked').val(),
                        val_thera_q4: $('#val_thera_q4 input[type="radio"]:checked').val(),
                        val_thera_q5: $('#val_thera_q5 input[type="radio"]:checked').val()
                    });
                    postAJAXArray('assets/backend/intakeForm/afterCarePost.php', afterCareIntakeForm);
                    // After form submission, present user with option to verify their inputted form through a simple page with the recent form data inputted. For now, redirect to form creation page after 1 second.
                    setTimeout(function () {
                        window.location = "b-aftercare.php";
                    }, 1000);
                } else {
                    toastr.error("Cannot submit form. Please check BADAC code and try again.", 'Error', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                }
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            handleTimePickers();
            frm_aftercare();

            // Form submission
            $("#aftercare_submitbtn").click(function () {
                $("#frm_aftercare").submit();

                if (frm_aftercare_isValid == 1) {
                    finalAftcrChk('assets/backend/intakeForm/getPersonID_afterCare.php', $('#val_aftcr_badac').val());
                } else {
                    toastr.error("Cannot submit form. Please check inputs and try again.", 'Error', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                }
            });
        }
    };
}();

jQuery(document).ready(function () {
    FormValidation.init();
});
