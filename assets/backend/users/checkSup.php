<?php
require_once ('../db/connect.php');
header("Content-type: application/json");
// Barangay selection limited to Davao for now
$json = $db->JsonBuilder()->rawQuery("SELECT rlnameusracc FROM tbl_users WHERE brgyusracc=" . $_GET['brgy'] . " AND 
lvlusracc=1");
if ($json) {
    print_r($json);
} else {
    throw new Exception("An error occured or a supervisor already exists for this barangay.\n");
    header('HTTP/1.1 500 Internal Server Error');
    header("Content-type: application/json");
    die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
}

?>