<?php
require_once('../db/connect.php');
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];
if ($verb == "POST") {
    $data = $_POST['sendData'];

    foreach ($data as $vars) {
        $updateArray = Array();
        $updateArray = Array(
            "val_aftcr_client" => $vars['val_aftcr_client'],
            "val_aftcr_addr" => $vars['val_aftcr_addr'],
            "val_aftcr_date" => $vars['val_aftcr_date'],
            "val_aftcr_contact" => $vars['val_aftcr_contact'];
            "val_aftcr_interviewer" => $vars['val_aftcr_interviewer'];
            "val_aftcr_notes" => $vars['val_aftcr_notes'],
            "val_med_q1" => $vars['val_med_q1'],
            "val_med_q2" => $vars['val_med_q2'],
            "val_med_q3" => $vars['val_med_q3'],
            "val_med_q4" => $vars['val_med_q4'],
            "val_med_q5" => $vars['val_med_q5'],
            "val_med_q6" => $vars['val_med_q6'],
            "val_psych_q1" => $vars['val_psych_q1'],
            "val_psych_q2" => $vars['val_psych_q2'],
            "val_psych_q3" => $vars['val_psych_q3'],
            "val_psych_q4" => $vars['val_psych_q4'],
            "val_psych_q5" => $vars['val_psych_q5'],
            "val_psych_q6" => $vars['val_psych_q6'],
            "val_psych_q7" => $vars['val_psych_q7'],
            "val_socio_q1" => $vars['val_socio_q1'],
            "val_socio_q2" => $vars['val_socio_q2'],
            "val_socio_q3" => $vars['val_socio_q3'],
            "val_socio_q4" => $vars['val_socio_q4'],
            "val_socio_q5" => $vars['val_socio_q5'],
            "val_socio_q6" => $vars['val_socio_q6'],
            "val_socio_q7" => $vars['val_socio_q7'],
            "val_spirit_q1" => $vars['val_spirit_q1'],
            "val_spirit_q2" => $vars['val_spirit_q2'],
            "val_self_q1" => $vars['val_self_q1'],
            "val_self_q2" => $vars['val_self_q2'],
            "val_thera_q1" => $vars['val_thera_q1'],
            "val_thera_q2" => $vars['val_thera_q2'],
            "val_thera_q3" => $vars['val_thera_q3'],
            "val_thera_q4" => $vars['val_thera_q4'],
            "val_thera_q5" => $vars['val_thera_q5']
        );
        $db->where ('person_id', $vars['val_aftcr_badac']);
        $dataUpdate = $db->update ('tbl_person_aftercare', $updateArray);
        if ($dataUpdate) {
            echo '1';
        } else {
            throw new Exception("An error occured or the user already exists. Try to use a different control number.\n");
            header('HTTP/1.1 500 Internal Server Error');
            header("Content-type: application/json");
            die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
        }
    }
}
?>