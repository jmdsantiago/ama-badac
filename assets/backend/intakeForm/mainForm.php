<?php
require_once('../db/connect.php');
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];
if ($verb == "POST") {
    $data = $_POST['sendData'];

    foreach ($data as $vars) {
        $insertArray = Array();
        $insertArray = Array("form_id" => rand(1, 65000),
            "form_encoderid" => $vars['form_encoderid'],
            "person_id" => $vars['person_id'],
            "val_ctrl_brgy" => $vars['val_ctrl_brgy'],
            "val_ctrl_dateadm" => $vars['val_ctrl_dateadm'],
            "val_bg_fname" => $vars['val_bg_fname'],
            "val_bg_mname" => $vars['val_bg_mname'],
            "val_bg_lname" => $vars['val_bg_lname'],
            "val_bg_addr" => $vars['val_bg_addr'],
            "val_bg_type" => $vars['val_bg_type'],
            "val_bg_contact" => $vars['val_bg_contact'],
            "val_bg_email" => $vars['val_bg_email'],
            "val_bg_surr_date" => $vars['val_bg_surr_date'],
            "val_bg_surr_time" => $vars['val_bg_surr_time'],
            "val_bg_age" => $vars['val_bg_age'],
            "val_bg_dob" => $vars['val_bg_dob'],
            "val_bg_sex" => $vars['val_bg_sex'],
            "val_bg_civstat" => $vars['val_bg_civstat'],
            "val_bg_edu" => $vars['val_bg_edu'],
            "val_bg_nation" => $vars['val_bg_nation'],
            "val_bg_relg" => $vars['val_bg_relg'],
            "val_bg_skill" => $vars['val_bg_skill'],
            "val_school_yrs" => $vars['val_school_yrs'],
            "val_school_date" => $vars['val_school_date'],
            "val_work_prerehab" => $vars['val_work_prerehab'],
            "val_fam_siblings" => $vars['val_fam_siblings'],
            "val_fam_ordinal" => $vars['val_fam_ordinal'],
            "val_fam_income" => $vars['val_fam_income'],
            "val_fam_dadname" => $vars['val_fam_dadname'],
            "val_fam_dadocc" => $vars['val_fam_dadocc'],
            "val_fam_momname" => $vars['val_fam_momname'],
            "val_fam_momocc" => $vars['val_fam_momocc'],
            "val_fam_spousename" => $vars['val_fam_spousename'],
            "val_fam_spouseocc" => $vars['val_fam_spouseocc'],
            "val_fam_spouse_addr" => $vars['val_fam_spouse_addr'],
            "val_drg_expense" => $vars['val_drg_expense'],
            "val_drg_agefirst" => $vars['val_drg_agefirst'],
            "val_drg_datelast" => $vars['val_drg_datelast'],
            "val_drg_lengthuse" => $vars['val_drg_lengthuse'],
            "val_drg_place" => $vars['val_drg_place'],
            "val_drg_reason" => $vars['val_drg_reason'],
            "val_trt_numadm" => $vars['val_trt_numadm'],
            "val_trt_freq" => $vars['val_trt_freq'],
            "val_trt_means" => $vars['val_trt_means']
        );
        $dataInsert = $db->insert('tbl_intakeform', $insertArray);
        if ($dataInsert) {
            // Do something with val_ctrl_photo
            // Check if barangay directory already exists
            $dirname = $vars["val_ctrl_brgy"];
            $dirPath = "../../intakeForm/data/images/" . $dirname . "/";
            if (!file_exists($dirPath)) {
                mkdir($dirPath, 0755);
                echo "The directory was successfully created.\n";
            } else {
                throw new Exception("The user already exists. Please use a different control number.\n");
                header('HTTP/1.1 500 Internal Server Error');
                header("Content-type: application/json");
                die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
            }
            $img = $vars["val_ctrl_photo"];
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $fileData = base64_decode($img);
            $myfile = fopen($dirPath . $vars['person_id'] . '.jpg', 'wb') or die("Unable to open file or file already exists!");
            fwrite($myfile, $fileData);
            fclose($myfile);
            echo '1';
        } else {
            throw new Exception("An error occured or the user already exists. Try to use a different control number.\n");
            header('HTTP/1.1 500 Internal Server Error');
            header("Content-type: application/json");
            die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
        }
    }
}
?>