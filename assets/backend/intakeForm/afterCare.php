<?php
require_once('../db/connect.php');
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];
if ($verb == "POST") {
    $data = $_POST['sendData'];

    foreach ($data as $vars) {
        $insertArray = Array();
        $insertArray = Array("num_id" => rand(1, 65000),
            "person_id" => $vars['person_id'],
            "brgy_id" => $vars['brgy_id'],
            "val_aftcr_client" => 'NA',
            "val_aftcr_addr" => 'NA',
            "val_aftcr_date" => 'NA',
            "val_aftcr_notes" => 'NA',
            "val_med_q1" => 'N',
            "val_med_q2" => 'N',
            "val_med_q3" => 'N',
            "val_med_q4" => 'N',
            "val_med_q5" => 'N',
            "val_med_q6" => 'N',
            "val_psych_q1" => 'N',
            "val_psych_q2" => 'N',
            "val_psych_q3" => 'N',
            "val_psych_q4" => 'N',
            "val_psych_q5" => 'N',
            "val_psych_q6" => 'N',
            "val_psych_q7" => 'N',
            "val_socio_q1" => 'N',
            "val_socio_q2" => 'N',
            "val_socio_q3" => 'N',
            "val_socio_q4" => 'N',
            "val_socio_q5" => 'N',
            "val_socio_q6" => 'N',
            "val_socio_q7" => 'N',
            "val_spirit_q1" => 'N',
            "val_spirit_q2" => 'N',
            "val_self_q1" => 'N',
            "val_self_q2" => 'N',
            "val_thera_q1" => 'N',
            "val_thera_q2" => 'N',
            "val_thera_q3" => 'N',
            "val_thera_q4" => 'N',
            "val_thera_q5" => 'N'
        );
        $dataInsert = $db->insert('tbl_person_aftercare', $insertArray);
        if ($dataInsert) {
            echo '1';
        } else {
            throw new Exception("An error occured or the user already exists. Try to use a different control number.\n");
            header('HTTP/1.1 500 Internal Server Error');
            header("Content-type: application/json");
            die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
        }
    }
}
?>