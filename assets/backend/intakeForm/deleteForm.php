<?php 
require_once ('../db/connect.php');
header("Content-type: application/json");
$verb = $_SERVER["REQUEST_METHOD"];
if($verb == "POST")
	{
		$data = $_POST['id'];
		$db->where('person_id', $data);
		$db->delete('tbl_person_admission');
		$db->where('person_id', $data);
		$db->delete('tbl_person_admissionother');
		$db->where('person_id', $data);
		$db->delete('tbl_person_drugsource');
		$db->where('person_id', $data);
		$db->delete('tbl_person_drugsourceother');
		$db->where('person_id', $data);
		$db->delete('tbl_person_drugused');
		$db->where('person_id', $data);
		$db->delete('tbl_person_freqother');
		$db->where('person_id', $data);
		$db->delete('tbl_person_livarr');
		$db->where('person_id', $data);
		$db->delete('tbl_person_livarrother');
		$db->where('person_id', $data);
		$db->delete('tbl_person_skills');
		$db->where('person_id', $data);
		$db->delete('tbl_person_substances');
        $db->where('person_id', $data);
        $db->delete('tbl_person_aftercare');

        $getBrgy = $db->rawQueryValue('select val_ctrl_brgy from tbl_intakeform where person_id=? limit 1', Array($_POST['id']));;
        $dirname = $getBrgy;
        $dirPath = "../../intakeForm/data/images/" . $dirname . "/";
        $imgPath = $dirPath . $_POST['id'] . '.jpg';
        if (file_exists($dirPath)) {
            if (file_exists($imgPath)) {
                unlink($imgPath);
                rmdir($dirPath);
            }
            else {
                echo false;
            }
        }

        $db->where('person_id', $data);

		if($db->delete('tbl_intakeform')) {
			echo '1';
		} else {
		    echo false;
		}
	}
?>