<?php
require_once('../db/connect.php');
$verb = $_SERVER["REQUEST_METHOD"];
if ($verb == "POST") {
    $data = $_POST['sendData'];
    foreach ($data as $vars) {
        $insertArray = Array();
        $insertArray = Array("num_id" => rand(1, 65000),
            "person_id" => $vars['person_id'],
            "brgy_id" => $vars['brgy_id'],
            "val_name" => $vars['livarrother_name']
        );
        $dataInsert = $db->insert('tbl_person_livarrother', $insertArray);
        if ($dataInsert) {
            echo true;
        } else {
            throw new Exception("The user already exists. Please use a different control number.\n");
            header('HTTP/1.1 500 Internal Server Error');
            header("Content-type: application/json");
            die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
        }
    }
}
?>