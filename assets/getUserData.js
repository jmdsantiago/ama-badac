/***
Function for getting personalized user data upon successful login. Retrieve from PHP sessions using AJAX.
***/

var getUserData = function() {

    var getDataForElement = function(element, url) {
        $.get(url, function(data, status){
            $(element).text(data);
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            getDataForElement('#page_usrname','assets/backend/page/username.php');
            getDataForElement('#page_usrbrgy','assets/backend/page/barangay.php');
        }

    };

}();

$(document).ready(function() {
    getUserData.init();
});
