var intakeFormValidation = function() {

    var getInputFields = function(inputField) {

        if (inputField == "person_id" || inputField == "val_school_yrs" || inputField == "val_bg_age" || inputField == "val_fam_siblings" || inputField == "val_fam_income" || inputField == "val_drg_expense" ||
            inputField == "val_drg_agefirst" || inputField == "frm_edit_ctrlnum" || inputField == "val_aftcr_badac") {
            // numbers only
            $("#" + inputField).inputmask('Regex', {
                regex: "[0-9]{2,20}"
            });
        } else if (inputField == "val_aftcr_notes" || inputField == "val_aftcr_addr" || inputField == "val_fam_spouse_addr" || inputField == "val_bg_addr") {
            // Address fields
            $("#" + inputField).inputmask('Regex', {
                regex: "[0-9a-zA-Z\\s.,]{2,150}"
            });
        } else if (inputField == "val_aftcr_date" || inputField == "val_bg_surr_date" || inputField == "val_ctrl_dateadm" || inputField == "val_bg_dob" || inputField == "val_school_date" || inputField == "val_drg_datelast") {
            // Date fields
            $("#" + inputField).inputmask('Regex', {
                regex: "[0-9/]{2,10}"
            });
        } else if (inputField == "passusracc" || inputField == "usr_edit_pass" || inputField == "nameusracc") {
            // Password fields
            $("#" + inputField).inputmask('Regex', {
                regex: "[0-9a-zA-Z]{2,20}"
            });
        } else if (inputField == "val_bg_email") {
            // Email fields
            $("#" + inputField).inputmask('Regex', {
                regex: "[0-9a-zA-Z@\\s.,]{2,20}"
            });
        } else if (inputField == "val_bg_surr_time") {
            // Time fields
            $("#" + inputField).inputmask('Regex', {
                regex: "[0-9:AMPMampm\\s]{2,10}"
            });
        } else if (inputField == "val_bg_contact" || inputField == "val_aftcr_contact") {
            // Contact number fields
            $("#" + inputField).inputmask('Regex', {
                regex: "[0-9]{2,20}"
            });
        }
        else {
            // All other input fields
            $("#" + inputField).inputmask('Regex', {
                regex: "[0-9a-zA-Z\\s]{2,150}"
            });
        }

    };

    return {
        //main function to initiate the module
        init: function() {
            $("form :input:text").each(function() {
                getInputFields($(this).attr('id'));
            });
            $("form :input:password").each(function() {
                getInputFields($(this).attr('id'));
            });

        }

    };

}();

$(document).ready(function() {
    intakeFormValidation.init();
});
