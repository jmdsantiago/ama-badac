var Login = function() {

    var handleLogin = function() {

        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                }
            },

            messages: {
                username: {
                    required: "Username is required."
                },
                password: {
                    required: "Password is required."
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function(form) {
                formSubmit(); // form validation success, call ajax form submit
            }
        });

        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    formSubmit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    };

    var formSubmit = function() {
        var loginArr = [];
        var testArr = [];
        var username = $('#login_username').val();
        var password = $('#login_password').val();
        loginArr.push({ nameusracc: username, passusracc: password });
        $.ajax({
            type: "POST",
            url: "assets/backend/login/login.php",
            data: { login: loginArr },
            success: function(data) {
                // check if returned data is empty
                if (jQuery.isEmptyObject(data)) {
                    $('.alert-danger', $('.login-form')).show();
                } else {
                    var retusrname = data[0]['nameusracc'];
                    var retpasswrd = data[0]['passusracc'];
                    if (retusrname == username && retpasswrd == password) {
                        $('.alert-danger', $('.login-form')).hide();
                        $('.alert-success', $('.login-form')).show();
                        Cookies.set('usr_id', data[0]['nameusracc']), { expires: 1 };
                        Cookies.set('rlnameusracc', data[0]['rlnameusracc']), { expires: 1 };
                        Cookies.set('lvlusracc', data[0]['lvlusracc']), { expires: 1 };
                        Cookies.set('brgyusracc', data[0]['brgyusracc']), { expires: 1 };
                        setTimeout(function() { window.location = "index.php"; }, 500);
                    } else {
                        $('.alert-danger', $('.login-form')).show();
                    }
                }
            },
            // variable server error functions
            error: function(xhr, ajaxOptions, thrownError) {
                console.log("Server status: " + xhr.status);
                console.log("Server response: " + xhr.responseText);
                console.log("Server error: " + thrownError);
            }
        });
    };

    return {
        //main function to initiate the module
        init: function() {

            handleLogin();

            $('#login_submit').click(function() {
                formSubmit();
            });

        }

    };

}();

jQuery(document).ready(function() {
    Login.init();
});
