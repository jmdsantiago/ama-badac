var FormValidation = function () {

    var supCheck = 0;

    var usr_val = function () {

        if (!$('#nameusracc').val() || !$('#passusracc').val() || !$('#rlnameusracc').val() || !$('#lvlusracc').val() || !$('#brgyusracc').val()) {
            toastr.warning("Please enter all the required values.", 'Warning', {timeOut: 9000, extendedTimeOut: 9000});
        } else {
            var usrAddArr = [];
            usrAddArr.push({
                nameusracc: $('#nameusracc').val(),
                passusracc: $('#passusracc').val(),
                rlnameusracc: $('#rlnameusracc').val(),
                lvlusracc: $('#lvlusracc').val(),
                brgyusracc: $('#brgyusracc').val()
            });
            postAJAXArray('assets/backend/users/userAdd.php', usrAddArr);

        }
    };

    var usr_val_sup = function () {

        if (!$('#nameusracc').val() || !$('#passusracc').val() || !$('#rlnameusracc').val()) {
            toastr.warning("Please enter all the required values.", 'Warning', {timeOut: 9000, extendedTimeOut: 9000});
        } else {
            var usrAddArr = [];
            usrAddArr.push({
                nameusracc: $('#nameusracc').val(),
                passusracc: $('#passusracc').val(),
                rlnameusracc: $('#rlnameusracc').val(),
                lvlusracc: '2',
                brgyusracc: Cookies.get('brgyusracc')
            });
            postAJAXArray('assets/backend/users/userAdd.php', usrAddArr);

        }
    };

    // Function for submitting arrays via AJAX
    var postAJAXArray = function (sendURL, sendData) {
        $.post({
            url: sendURL,
            data: {sendData: sendData},
            success: function (data) {
                if (data != '1') {
                    toastr.warning("Some data may not have been sent properly. Please edit the form after this.", 'AJAX Error', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                } else {
                    toastr.success('User added to database. Please wait while the page reloads.', 'Success', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.error('Error: User already exists in the database.', 'Error', {
                    timeOut: 9000,
                    extendedTimeOut: 9000
                });
                $('#nameusracc').val('');
                $('#passusracc').val('');
                $('#rlnameusracc').val('');
                $('#lvlusracc').val('');
                $('#brgyusracc').val('');
                console.log("Server status: " + xhr.status);
                console.log("Server response: " + xhr.responseText);
                console.log("Server error: " + thrownError);
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            $("#usr_add_btn").click(function () {
                var getUserType = Cookies.get('lvlusracc');
                var getSelectType = $('#lvlusracc').val();
                if (getUserType == '1') {
                    usr_val_sup();
                } else {
                    // Check if barangay already has supervisor
                    if (getSelectType == 1) {
                        $.ajax({
                            url: "assets/backend/users/checkSup.php",
                            data: { brgy: $('#brgyusracc').val() },
                            success: function(data) {
                                // check if returned data is empty
                                if (jQuery.isEmptyObject(data)) {
                                    usr_val();
                                } else {
                                    toastr.error("A supervisor already exists for this barangay.", 'Error', { timeOut: 9000, extendedTimeOut: 9000 });
                                }
                            },
                            // variable server error functions
                            error: function(xhr, ajaxOptions, thrownError) {
                                toastr.error("The server sent an error. Please try filling out the data and try submitting again.", 'Error', { timeOut: 9000, extendedTimeOut: 9000 });
                            }
                        });
                    }
                    else {
                        usr_val();
                    }

                }
            });
        }
    };
}();

jQuery(document).ready(function () {
    FormValidation.init();
});
