var formEditExt = function () {

    var frmDelAJAX = function (sendData) {
        var value = $('#frm_edit_ctrlnum').val();
        if (!value) {
            toastr.error('No value detected.', 'Error', {timeOut: 9000, extendedTimeOut: 9000});
        } else {
            $.post({
                url: 'assets/backend/intakeForm/deleteForm.php',
                dataType: 'json',
                data: {id: sendData},
                success: function (data) {
                    console.log(data);
                    if (jQuery.isEmptyObject(data)) {
                        toastr.success('Form deleted.', 'Success', {timeOut: 9000, extendedTimeOut: 9000});
                        $('#frm_edit_ctrlnum').val('');
                    } else {
                        toastr.error('Form does not exist.', 'Error', {timeOut: 9000, extendedTimeOut: 9000});
                        $('#frm_edit_ctrlnum').val('');
                    }
                },
                // variable server error functions
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr.error('Form does not exist.', 'Error', {timeOut: 9000, extendedTimeOut: 9000});
                    $('#frm_edit_ctrlnum').val('');
                }
            });
        }
    };

    var getFormID = function (sendData, elementID) {
        $.ajax({
            url: 'assets/backend/intakeForm/getEditSingleData.php',
            dataType: 'json',
            data: {id: sendData},
            success: function (data) {
                if (jQuery.isEmptyObject(data)) {
                    toastr.error("This control number does not exist!", 'Error', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                    $('#frm_edit_ctrlnum').val('');
                    $("#frm_del_submit").prop("disabled", false);
                } else {
                    var showThisElement = $('#frm_edit_portion').val();
                    var retdata = data[0][showThisElement];
                    toastr.success("Returned value for editing: " + retdata, 'Debugging Mode', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                    // Disable control number input and show the value to be edited
                    $("#intakeFormForEdit").find("*").show();
                    $("#intakeFormForEdit").find(".select2").hide();
                    $('#formEditContainer div').each(function () {
                        if (this.id == showThisElement) {
                            $("#frm_edit_ctrlnum").prop("disabled", true);
                            $('#' + showThisElement).show();
                            $('#' + showThisElement).prop("disabled", false);
                            if ($('#' + showThisElement).hasClass("dateElement")) {
                                $('#' + showThisElement + ' input').each(function () {
                                    $(this).val(retdata);
                                    toastr.success('Data retrieved successfully.', 'Debugging Mode', {
                                        timeOut: 9000,
                                        extendedTimeOut: 9000
                                    });
                                });
                            } else if ($('#' + showThisElement).hasClass("inputElement")) {
                                $('#' + showThisElement + ' input').each(function () {
                                    $(this).val(retdata);
                                    toastr.success('Data retrieved successfully.', 'Debugging Mode', {
                                        timeOut: 9000,
                                        extendedTimeOut: 9000
                                    });
                                });
                                // Implement getting values for these elements in a future build
                            } else if ($('#' + showThisElement).hasClass("radioBoxElement")) {

                            } else if ($('#' + showThisElement).hasClass("checkBoxElement")) {

                            } else if ($('#' + showThisElement).hasClass("selectElement")) {

                            } else {
                                //$('#frm_edit_ctrlnum').val('');
                                //$("#frm_edit_ctrlnum").prop("disabled", false);
                                //$("#intakeFormForEdit").find("*").hide();
                                //$("#frm_del_submit").prop("disabled", false);
                                if (showThisElement == 'val_bg_sex' || showThisElement == 'val_bg_civstat' || showThisElement == 'val_bg_edu' || showThisElement == 'val_fam_ordinal') {
                                    $('#' + showThisElement).find("*").show();
                                } else {
                                    getSelectData(showThisElement + '_edit');
                                }
                                //toastr.error('There was a problem retrieving data. Please try again.', 'Error', { timeOut: 9000, extendedTimeOut: 9000 });
                            }
                            //getSelectData($("#" + showThisElement + " select").prop('id'));
                            toastr.info('Element for editing is: ' + showThisElement, 'Debugging Mode', {
                                timeOut: 9000,
                                extendedTimeOut: 9000
                            });
                        } else {
                            // Disable other elements and hide them
                            $('#' + this.id).hide();
                            $('#' + this.id).prop("disabled", true);
                        }
                    });
                }
            },
            // variable server error functions
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("Server status: " + xhr.status);
                console.log("Server response: " + xhr.responseText);
                console.log("Server error: " + thrownError);
            }

        });
    };

    var getSelectData = function (getSelectBox) {
        $("#" + getSelectBox).select2({
            ajax: {
                url: "assets/backend/select/select_" + getSelectBox + ".php",
                tokenSeparators: [',', ' '],
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term // search term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (obj) {
                            return {id: obj.val_id, text: obj.val_name};
                        })
                    };
                }
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            // Hide container until edit form is submitted
            $("#intakeFormForEdit").find("*").hide();
            // Hide other elements until specified
            $("#formEditContainer").find("*").hide();
            // Disable all elements until specified
            $("#frm_edit_submit").click(function () {
                if (!$('#frm_edit_ctrlnum').val()) {
                    toastr.error("Please enter a control number.", 'Error', {timeOut: 9000, extendedTimeOut: 9000});
                } else {
                    $("#frm_del_submit").prop("disabled", true);
                    //$("#intakeFormForEdit").find(":input").prop("disabled", true);
                    // Get control number value for AJAX submission
                    var getFormArray = [];
                    getFormArray.push({id: $('#frm_edit_ctrlnum').val(), val: $('#frm_edit_portion').val()});
                    getFormID(getFormArray);
                }
            });

            $("#frm_edit_cancel").click(function () {
                // Hide container until edit form is submitted
                $("#intakeFormForEdit").find("*").hide();
                // Hide other elements until specified
                $("#formEditContainer").find("*").hide();
                $('#frm_edit_ctrlnum').val('');
                $("#frm_edit_ctrlnum").prop("disabled", false);
                $("#frm_del_submit").prop("disabled", false);
            });

            $("#frm_del_submit").click(function () {
                if (!$('#frm_edit_ctrlnum').val()) {
                    toastr.error("Please enter a control number.", 'Error', {timeOut: 9000, extendedTimeOut: 9000});
                } else {
                    frmDelAJAX($('#frm_edit_ctrlnum').val());
                    $('#frm_edit_ctrlnum').val('');
                }
            });

            /*     $("#frm_edit_ctrlnum").change(function() {
             $('.form-control.select2dynamic').each(function(i, obj) {
             getSelectData($(this).attr('id'));
             });
             });
             */
        }

    };

}();

jQuery(document).ready(function () {
    formEditExt.init();
});
