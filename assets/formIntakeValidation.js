var FormValidation = function () {

    // Variables for checking individual form validation; if all are 1 then pass entire form for submitting; if any are 0 then alert user for form checking
    var frm_intake_ctrl_isValid = 0;
    var frm_intake_bginfo_isValid = 0;
    var frm_intake_drguse_isValid = 0;
    var frm_intake_treat_isValid = 0;
    var video;
    var canvas;

    // Functions for validating individual forms
    var frm_intake_ctrl_val = function () {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var frm_intake_ctrlvar = $('#frm_intake_ctrl');
        var error2 = $('.alert-danger', frm_intake_ctrlvar);
        var success2 = $('.alert-success', frm_intake_ctrlvar);

        frm_intake_ctrlvar.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                person_id: {
                    minlength: 1,
                    required: true
                },
                val_ctrl_brgy: {
                    required: true
                },
                val_ctrl_dateadm: {
                    required: true
                }
            },

            messages: { // custom messages for radio buttons and checkboxes
                tester: {
                    required: "Please select at least 2 types of Service",
                    minlength: jQuery.validator.format("Please select  at least {0} types of Test")
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
                frm_intake_ctrl_isValid = 0;
            },

            errorPlacement: function (error, element) { // render error placement for each input type

                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({
                        'container': 'body'
                    }); // for other inputs, just perform default behavior
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");

            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                frm_intake_ctrl_isValid = 1;
            }
        });

        //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
        $('.select2me', frm_intake_ctrlvar).change(function () {
            frm_intake_ctrlvar.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

        //initialize datepicker
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
        $('.date-picker .form-control').change(function () {
            frm_intake_ctrlvar.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

    };

    var frm_intake_bginfo_val = function () {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var frm_intake_bginfo_var = $('#frm_intake_bginfo');
        var error2 = $('.alert-danger', frm_intake_bginfo_var);
        var success2 = $('.alert-success', frm_intake_bginfo_var);

        frm_intake_bginfo_var.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                val_bg_fname: {
                    minlength: 3,
                    required: true
                },
                val_bg_mname: {
                    minlength: 3,
                    required: true
                },
                val_bg_lname: {
                    minlength: 3,
                    required: true
                },
                val_bg_addr: {
                    minlength: 5,
                    required: true
                },
                val_bg_type: {
                    required: true
                },
                val_bg_contact: {
                    maxlength: 11,
                    number: true,
                    required: true
                },
                val_bg_email: {
                    email: true,
                    required: true
                },
                val_bg_surr_date: {
                    required: true
                },
                val_bg_surr_time: {
                    required: true
                },
                val_bg_age: {
                    number: true,
                    required: true
                },
                val_bg_dob: {
                    required: true
                },
                val_bg_sex: {
                    required: true
                },
                val_bg_civstat: {
                    required: true
                },
                val_bg_edu: {
                    required: true
                },
                val_bg_nation: {
                    required: true
                },
                val_bg_relg: {
                    required: true
                },
                val_school_yrs: {
                    number: true,
                    required: true
                },
                val_school_date: {
                    required: true
                },
                val_work_prerehab: {
                    required: true
                },
                val_bg_skill: {
                    minlength: 5,
                    required: true
                },
                val_fam_siblings: {
                    number: true,
                    required: true
                },
                val_fam_ordinal: {
                    required: true
                },
                person_livar: {
                    required: true,
                    minlength: 1
                },
                person_livarother: {
                    minlength: 5,
                    required: true
                },
                val_fam_income: {
                    number: true,
                    required: true
                },
                val_fam_dadname: {
                    minlength: 3,
                    required: true
                },
                val_fam_dadocc: {
                    required: true
                },
                val_fam_momname: {
                    minlength: 3,
                    required: true
                },
                val_fam_momocc: {
                    required: true
                },
                val_fam_spousename: {
                    minlength: 3,
                    required: true
                },
                val_fam_spouseocc: {
                    required: true
                },
                val_fam_spouse_addr: {
                    minlength: 5,
                    required: true
                }

            },

            messages: { // custom messages for radio buttons and checkboxes
                person_livar: {
                    required: "Please select at least 1",
                    minlength: jQuery.validator.format("Please select  at least {0}")
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
                frm_intake_bginfo_isValid = 0;
            },

            errorPlacement: function (error, element) { // render error placement for each input type

                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({
                        'container': 'body'
                    }); // for other inputs, just perform default behavior
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");

            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                frm_intake_bginfo_isValid = 1;
            }
        });

        //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
        $('.select2me', frm_intake_bginfo_var).change(function () {
            frm_intake_bginfo_var.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

        //initialize datepicker
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
        $('.date-picker .form-control').change(function () {
            frm_intake_bginfo_var.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

    };

    var frm_intake_drguse_val = function () {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var frm_intake_drguse_var = $('#frm_intake_drguse');
        var error2 = $('.alert-danger', frm_intake_drguse_var);
        var success2 = $('.alert-success', frm_intake_drguse_var);

        frm_intake_drguse_var.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                val_drg_expense: {
                    number: true,
                    required: true
                },
                val_drg_agefirst: {
                    number: true,
                    required: true
                },
                val_drg_datelast: {
                    required: true
                },
                val_drg_lengthuse: {
                    required: true
                },
                person_sourcedrug: {
                    required: true,
                    minlength: 1
                },
                person_sourcedrugother: {
                    required: true
                },
                val_drg_place: {
                    minlength: 3,
                    required: true
                },
                val_drg_reason: {
                    minlength: 3,
                    required: true
                },
                person_drugsused: {
                    required: true
                }

            },

            messages: { // custom messages for radio buttons and checkboxes
                person_sourcedrug: {
                    required: "Please select at least 1",
                    minlength: jQuery.validator.format("Please select  at least {0}")
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
                frm_intake_drguse_isValid = 0;
            },

            errorPlacement: function (error, element) { // render error placement for each input type

                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({
                        'container': 'body'
                    }); // for other inputs, just perform default behavior
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");

            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                frm_intake_drguse_isValid = 1;
                //form[0].submit(); // submit the form
            }
        });

        //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
        $('.select2me', frm_intake_drguse_var).change(function () {
            frm_intake_drguse_var.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

        //initialize datepicker
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
        $('.date-picker .form-control').change(function () {
            frm_intake_drguse_var.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

    };

    var frm_intake_treat_val = function () {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var frm_intake_treat_var = $('#frm_intake_treat');
        var error2 = $('.alert-danger', frm_intake_treat_var);
        var success2 = $('.alert-success', frm_intake_treat_var);

        frm_intake_treat_var.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                person_natadm: {
                    required: true,
                    minlength: 1
                },
                val_trt_numadm: {
                    required: true
                },
                person_natadmother: {
                    required: true,
                    minlength: 1
                },
                person_numadmother: {
                    minlength: 3,
                    required: true
                },
                person_drugsub: {
                    required: true
                },
                val_trt_freq: {
                    required: true,
                    minlength: 1
                },
                val_trt_freqother: {
                    minlength: 3,
                    required: true
                },
                val_trt_means: {
                    minlength: 3,
                    required: true
                }

            },

            messages: { // custom messages for radio buttons and checkboxes
                person_natadm: {
                    required: "Please select at least 1",
                    minlength: jQuery.validator.format("Please select  at least {0}")
                },
                val_trt_freq: {
                    required: "Please select at least 1",
                    minlength: jQuery.validator.format("Please select  at least {0}")
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success2.hide();
                error2.show();
                App.scrollTo(error2, -200);
                frm_intake_treat_isValid = 0;
            },

            errorPlacement: function (error, element) { // render error placement for each input type

                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({
                        'container': 'body'
                    }); // for other inputs, just perform default behavior
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");

            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                frm_intake_treat_isValid = 1;
                //form[0].submit(); // submit the form
            }
        });

        //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
        $('.select2me', frm_intake_treat_var).change(function () {
            frm_intake_treat_var.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

        //initialize datepicker
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
        $('.date-picker .form-control').change(function () {
            frm_intake_treat_var.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
        });

    };
    // Function for displaying hidden input fields in "Other" checkboxes
    var showHideOtherInputFieldsChk = function (mainInput, fieldHide) {
        $("#" + mainInput).click(function () {
            if ($("#" + mainInput).is(':checked')) {
                $("#" + fieldHide).show();
                $("#" + fieldHide).attr("disabled", false);
            } else {
                $("#" + fieldHide).hide();
                $("#" + fieldHide).attr("disabled", true);
            }
        });

    };
    // Function for hiding input fields in "Other" radioboxes
    var hideOtherInputFieldsRad = function (mainInput, fieldHide) {
        $("#" + mainInput).click(function () {
            if ($("#" + mainInput).is(':checked')) {
                $("#" + fieldHide).hide();
                $("#" + fieldHide).attr("disabled", true);
            }
        });
    };
    // Function for showing input fields in "Other" radioboxes
    var showOtherInputFieldsRad = function (mainInput, fieldHide) {
        $("#" + mainInput).click(function () {
            if ($("#" + mainInput).is(':checked')) {
                $("#" + fieldHide).show();
                $("#" + fieldHide).attr("disabled", false);
            }
        });
    };

    var handleTimePickers = function () {

        if (jQuery().timepicker) {
            $('.timepicker-default').timepicker({
                autoclose: true,
                showSeconds: true,
                minuteStep: 1
            });

            $('.timepicker-no-seconds').timepicker({
                autoclose: true,
                minuteStep: 5
            });

            $('.timepicker-24').timepicker({
                autoclose: true,
                minuteStep: 5,
                showSeconds: false,
                showMeridian: false
            });

            // handle input group button click
            $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function (e) {
                e.preventDefault();
                $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
            });

            // Workaround to fix timepicker position on window scroll
            $(document).scroll(function () {
                $('#form_modal4 .timepicker-default, #form_modal4 .timepicker-no-seconds, #form_modal4 .timepicker-24').timepicker('place'); //#modal is the id of the modal
            });
        }
    };

    // Function for submitting arrays via AJAX
    var postAJAXArray = function (sendURL, sendData) {
        $.post({
            url: sendURL,
            data: {sendData: sendData},
            success: function (data) {
                if (jQuery.isEmptyObject(data)) {
                    //toastr.warning("Some data may not have been sent properly. Please edit the form after this.", 'AJAX Error', { timeOut: 9000, extendedTimeOut: 9000 });
                } else {
                    //toastr.info('Notice: A part of the form has been processed by the server.', 'Debugging mode', { timeOut: 9000, extendedTimeOut: 9000 });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                toastr.error("The server sent an error. Please try filling out the data and try submitting again.", 'Error', {
                    timeOut: 9000,
                    extendedTimeOut: 9000
                });
            }
        });
    };
    // Function to pass multiple choice elements to arrays then to AJAX
    var pass2Array = function (elementData, personIDVal, brgy, tblVal, submitURL) {
        var Arr = [];
        if ($('#' + elementData).is(":checkbox") || $('#' + elementData).is(":radio")) {
            $('#' + elementData).each(function () {
                if ($(this).is(':checked')) {
                    Arr.push({
                        num_id: '',
                        person_id: personIDVal,
                        brgy_id: brgy,
                        tblVal: $(this).val()
                    });
                } else { //remove this in production (DEBUGGING PURPOSE LANG)
                    toastr.info("\"" + elementData + "\" does not have any checked values.", 'Debugging mode', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                }
            });
            postAJAXArray(submitURL, Arr);
        } else if ($('#' + elementData).is(":selected") || ($('#' + elementData).is(":text") && $('#' + elementData).not(":disabled"))) {
            $('#' + elementData).each(function () {
                Arr.push({
                    num_id: '',
                    person_id: personIDVal,
                    brgy_id: brgy,
                    tblVal: $(this).val()
                });
            });
            postAJAXArray(submitURL, Arr);
        } else { //remove this in production (DEBUGGING PURPOSE LANG)
            toastr.info("\"" + elementData + "\" is not a valid checkbox,radiobox, or select field.", 'Debugging mode', {
                timeOut: 9000,
                extendedTimeOut: 9000
            });
        }
    };

    var getUserData = function (url, type) {
        $.get(url, function (data, status) {
            type = data;
        });
    };

    var initiateWebCam = function () {
        video = document.querySelector('#from_cam');
        canvas = window.canvas = document.querySelector('#val_ctrl_photo');

        var constraints = {
            audio: false,
            video: true
        };

        function handleSuccess(stream) {
            window.stream = stream; // make stream available to browser console
            video.srcObject = stream;
        }

        function handleError(error) {
            toastr.error('You may not have enabled access to your camera or you do not have a webcam attached. Please enable it.', 'Webcam Error', {
                timeOut: 9000,
                extendedTimeOut: 9000
            });
        }

        navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);

    };

    return {
        //main function to initiate the module
        init: function () {
            handleTimePickers();
            frm_intake_ctrl_val();
            frm_intake_bginfo_val();
            frm_intake_drguse_val();
            frm_intake_treat_val();
            initiateWebCam();

            // Hide canvas for saving JPEG
            $("#val_ctrl_photo").hide();
            $("#btn_changeimg").hide();

            $("#btn_capimg").click(function () {
                canvas.width = 320;
                canvas.height = 240;
                canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
                $("#val_ctrl_photo").show();
                $("#from_cam").hide();
                toastr.success("Image captured!", {timeOut: 9000, extendedTimeOut: 9000});
                $("#btn_changeimg").show();
                $("#btn_capimg").hide();
            });

            $("#btn_changeimg").click(function () {
                $("#val_ctrl_photo").hide();
                $("#from_cam").show();
                toastr.info("Please re-capture an image!", {timeOut: 9000, extendedTimeOut: 9000});
                $("#btn_changeimg").hide();
                $("#btn_capimg").show();
            });

            // Hide "Other" input boxes on module initialization
            $("#person_livarother").hide();
            $("#person_sourcedrugother").hide();
            $("#person_natadmother").hide();
            $("#person_numadmother").hide();
            $("#val_trt_freqother").hide();

            $('#person_id').val(Math.floor((Math.random() * 65000) + 1));

            // Show/hide "Other" in checkboxes
            showHideOtherInputFieldsChk('person_livar7', 'person_livarother');
            showHideOtherInputFieldsChk('person_sourcedrug6', 'person_sourcedrugother');
            showHideOtherInputFieldsChk('person_natadm7', 'person_natadmother');

            // Show/hide "Other" in radio boxes
            hideOtherInputFieldsRad('person_freqdrug1', 'val_trt_freqother');
            hideOtherInputFieldsRad('person_freqdrug2', 'val_trt_freqother');
            hideOtherInputFieldsRad('person_freqdrug3', 'val_trt_freqother');
            hideOtherInputFieldsRad('person_freqdrug4', 'val_trt_freqother');
            hideOtherInputFieldsRad('person_freqdrug5', 'val_trt_freqother');
            showOtherInputFieldsRad('person_freqdrug6', 'val_trt_freqother');

            // Form submission
            $("#frm_intake_btn").click(function () {
                var personID;
                var encoderID;
                var regionID;
                var dataURL = canvas.toDataURL();
                var brgyID = $('#val_ctrl_brgy').val();
                $("#frm_intake_ctrl").submit();
                $("#frm_intake_bginfo").submit();
                $("#frm_intake_drguse").submit();
                $("#frm_intake_treat").submit();

                if (frm_intake_ctrl_isValid == 1 && frm_intake_bginfo_isValid == 1 && frm_intake_drguse_isValid == 1 && frm_intake_treat_isValid == 1 && $('#btn_changeimg').is(":visible")) {
                    // if (frm_intake_drguse_isValid == 1) { //enable during testing/debugging
                    // Get person ID from control number
                    personID = $('#person_id').val();
                    encoderID = Cookies.get('usr_id');
                    toastr.info("Encoder ID is: " + encoderID, 'Debugging mode', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });

                    // Submission of main form
                    var mainIntakeFormArr = [];
                    mainIntakeFormArr.push({
                        form_id: '',
                        form_encoderid: encoderID,
                        person_id: personID,
                        val_ctrl_brgy: $('#val_ctrl_brgy').val(),
                        val_ctrl_dateadm: $('#val_ctrl_dateadm').val(),
                        val_ctrl_photo: dataURL,
                        val_bg_fname: $('#val_bg_fname').val(),
                        val_bg_mname: $('#val_bg_mname').val(),
                        val_bg_lname: $('#val_bg_lname').val(),
                        val_bg_addr: $('#val_bg_addr').val(),
                        val_bg_type: $('#val_bg_type').val(),
                        val_bg_contact: $('#val_bg_contact').val(),
                        val_bg_email: $('#val_bg_email').val(),
                        val_bg_surr_date: $('#val_bg_surr_date').val(),
                        val_bg_surr_time: $('#val_bg_surr_time').val(),
                        val_bg_age: $('#val_bg_age').val(),
                        val_bg_dob: $('#val_bg_dob').val(),
                        val_bg_sex: $('#val_bg_sex').val(),
                        val_bg_civstat: $('#val_bg_civstat').val(),
                        val_bg_edu: $('#val_bg_edu').val(),
                        val_bg_nation: $('#val_bg_nation').val(),
                        val_bg_relg: $('#val_bg_relg').val(),
                        val_bg_skill: $('#val_bg_skill').val(),
                        val_school_yrs: $('#val_school_yrs').val(),
                        val_school_date: $('#val_school_date').val(),
                        val_work_prerehab: $('#val_work_prerehab').val(),
                        val_fam_siblings: $('#val_fam_siblings').val(),
                        val_fam_ordinal: $('#val_fam_ordinal').val(),
                        val_fam_income: $('#val_fam_income').val(),
                        val_fam_dadname: $('#val_fam_dadname').val(),
                        val_fam_dadocc: $('#val_fam_dadocc').val(),
                        val_fam_momname: $('#val_fam_momname').val(),
                        val_fam_momocc: $('#val_fam_momocc').val(),
                        val_fam_spousename: $('#val_fam_spousename').val(),
                        val_fam_spouseocc: $('#val_fam_spouseocc').val(),
                        val_fam_spouse_addr: $('#val_fam_spouse_addr').val(),
                        val_drg_expense: $('#val_drg_expense').val(),
                        val_drg_agefirst: $('#val_drg_agefirst').val(),
                        val_drg_datelast: $('#val_drg_datelast').val(),
                        val_drg_lengthuse: $('#val_drg_lengthuse input[type="radio"]:checked').val(),
                        val_drg_place: $('#val_drg_place').val(),
                        val_drg_reason: $('#val_drg_reason').val(),
                        val_trt_numadm: $('#val_trt_numadm input[type="radio"]:checked').val(),
                        val_trt_freq: $('#val_trt_freq input[type="radio"]:checked').val(),
                        val_trt_means: $('#val_trt_means').val()
                    });
                    postAJAXArray('assets/backend/intakeForm/mainForm.php', mainIntakeFormArr);

                    var afterCareFormArr = [];
                    afterCareFormArr.push({
                        num_id: '',
                        person_id: personID,
                        brgy_id: brgyID
                    });
                    postAJAXArray('assets/backend/intakeForm/afterCare.php', afterCareFormArr);

                    // Submission of person-specific data
                    var submitArrayPersonData = [];
                    submitArrayPersonData.push({
                        elem: 'person_livar input:checked',
                        pid: personID,
                        brgy: brgyID,
                        valueid: 'livarr_type',
                        url: 'assets/backend/intakeForm/person_livar_chk.php'
                    });
                    if ($('#person_livar input[type="checkbox"]').eq(6).prop('checked') === true) {
                        submitArrayPersonData.push({
                            elem: 'person_livarother',
                            pid: personID,
                            brgy: brgyID,
                            valueid: 'livarrother_name',
                            url: 'assets/backend/intakeForm/person_livar_other.php'
                        });
                    }
                    submitArrayPersonData.push({
                        elem: 'person_sourcedrug input:checked',
                        pid: personID,
                        brgy: brgyID,
                        valueid: 'drugsource_val',
                        url: 'assets/backend/intakeForm/person_sourcedrug.php'
                    });
                    if ($('#person_sourcedrug input[type="checkbox"]').eq(5).prop('checked') === true) {
                        submitArrayPersonData.push({
                            elem: 'person_sourcedrugother',
                            pid: personID,
                            brgy: brgyID,
                            valueid: 'drugsourceother_name',
                            url: 'assets/backend/intakeForm/person_sourcedrugother.php'
                        });
                    }
                    submitArrayPersonData.push({
                        elem: 'person_drugsused option:selected',
                        pid: personID,
                        brgy: brgyID,
                        valueid: 'val_name',
                        url: 'assets/backend/intakeForm/person_drugsused.php'
                    });
                    submitArrayPersonData.push({
                        elem: 'person_natadm input:checked',
                        pid: personID,
                        brgy: brgyID,
                        valueid: 'adm_val',
                        url: 'assets/backend/intakeForm/person_natadm.php'
                    });
                    if ($('#person_natadm input[type="checkbox"]').eq(6).prop('checked') === true) {
                        submitArrayPersonData.push({
                            elem: 'person_natadmother',
                            pid: personID,
                            brgy: brgyID,
                            valueid: 'admother_val',
                            url: 'assets/backend/intakeForm/person_natadmother.php'
                        });
                    }
                    submitArrayPersonData.push({
                        elem: 'person_drugsub option:selected',
                        pid: personID,
                        brgy: brgyID,
                        valueid: 'substance_id',
                        url: 'assets/backend/intakeForm/person_drugsub.php'
                    });
                    /*
                     submitArrayPersonData.push({ elem: 'val_trt_freq input:checked', pid: personID, valueid: 'nnnnnn', url: 'assets/backend/intakeForm/val_trt_freq.php' });
                     if ($('#val_trt_freq input[type="radio"]').eq(5).prop('checked') === true) {
                     submitArrayPersonData.push({ elem: 'val_trt_freqother', pid: personID, valueid: 'nnnnnn', url: 'assets/backend/intakeForm/val_trt_freqother.php' });
                     }
                     */

                    // Process array and send through AJAX
                    for (var key in submitArrayPersonData) {
                        pass2Array(submitArrayPersonData[key]['elem'], submitArrayPersonData[key]['pid'], submitArrayPersonData[key]['brgy'], submitArrayPersonData[key]['valueid'], submitArrayPersonData[key]['url']);
                    }
                    // After form submission, present user with option to verify their inputted form through a simple page with the recent form data inputted. For now, redirect to form creation page after 1 second.
                    setTimeout(function () {
                        window.location = "a-form.php";
                    }, 1000);
                } else {
                    toastr.error("Cannot submit form. Please check inputs and try again.", 'Error', {
                        timeOut: 9000,
                        extendedTimeOut: 9000
                    });
                }
            });
        }
    };
}();

jQuery(document).ready(function () {
    FormValidation.init();
});
